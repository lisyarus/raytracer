#include <geom/primitives/vector.hpp>
#include <geom/primitives/point.hpp>
#include <geom/primitives/matrix.hpp>
#include <geom/primitives/interval.hpp>
#include <geom/primitives/rectangle.hpp>
#include <geom/primitives/ray.hpp>
#include <geom/primitives/hyperplane.hpp>
#include <geom/primitives/sphere.hpp>
#include <geom/transform/homogeneous.hpp>
#include <geom/operations/vector.hpp>
#include <geom/operations/point.hpp>
#include <geom/operations/matrix.hpp>
#include <geom/operations/interval.hpp>
#include <geom/operations/rectangle.hpp>
#include <geom/operations/intersect/ray_rectangle.hpp>
#include <geom/operations/intersect/ray_hyperplane.hpp>
#include <geom/operations/intersect/ray_sphere.hpp>
#include <geom/operations/intersect/ray_simplex.hpp>
#include <geom/operations/affine_hull.hpp>
#include <geom/primitives/ndarray.hpp>
#include <geom/io/interval.hpp>
#include <geom/io/vector.hpp>

#include <util/pretty_print.hpp>
#include <util/moving_average.hpp>

#include <memory>
#include <random>
#include <chrono>
#include <iostream>
#include <vector>
#include <atomic>
#include <thread>

using scalar = float;
using index = std::uint32_t;

using point    = geom::point<scalar, 3>;
using vector   = geom::vector<scalar, 3>;
using interval = geom::interval<scalar>;
using plane    = geom::hyperplane<scalar, 3>;
using sphere   = geom::sphere<scalar, 2>;
using triangle = geom::simplex<point, 2>;
using ray      = geom::ray<scalar, 3>;
using box      = geom::rectangle<scalar, 3>;

using byte = unsigned char;
using pixel = geom::vector<byte, 3>;
using texture = geom::ndarray<pixel, 2>;

static constexpr scalar inf = std::numeric_limits<scalar>::infinity();
static constexpr scalar pi = static_cast<scalar>(geom::pi);

static const box color_box {{
	{0, 1},
	{0, 1},
	{0, 1}
}};

vector mult (vector const & x, vector const & y)
{
	return { x[0] * y[0], x[1] * y[1], x[2] * y[2] };
}

using rng = std::minstd_rand;

scalar random (rng & gen)
{
	return std::uniform_real_distribution<scalar>{0, 1}(gen);
}

std::size_t random_index (rng & gen, std::size_t max)
{
	return std::uniform_int_distribution<std::size_t>{0, max - 1}(gen);
}

scalar random_normal (rng & gen)
{
	return std::normal_distribution<scalar>{}(gen);
}

vector random_unit_sphere (rng & gen)
{
	vector v;
	v[0] = random_normal(gen);
	v[1] = random_normal(gen);
	v[2] = random_normal(gen);
	return geom::normalized(v);
}

vector random_unit_ball (rng & gen)
{
	return random_unit_sphere(gen) * std::cbrt(random(gen));
}

vector random_unit_halfsphere (rng & gen, vector const & n)
{
	vector v = random_unit_sphere(gen);
	if (v * n >= 0)
		return v;
	else
		return -v;
}

std::pair<scalar, scalar> random_triangle (rng & gen)
{
	scalar u = random(gen);
	scalar v = random(gen);

	scalar const d = u + v - scalar(1);

	if (d > scalar(0))
	{
		u -= d;
		v -= d;
	}

	return {u, v};
}

struct material
{
	vector color;
	vector emission = { 0.f, 0.f, 0.f };
	scalar reflectance = 0.f; // 0..1
	scalar reflection_fuzziness = 0.f;
};

struct scene
{
	std::vector<point> points;
	std::vector<vector> normals;
	std::vector<std::array<index, 3>> triangle_vertices;

	std::vector<material> materials;
	std::vector<index> triangle_materials;
};

struct camera
{
	point position;
	vector x, y, z;
	scalar fov_x;

	::ray ray (scalar px, scalar py, scalar res_x, scalar res_y) const
	{
		scalar const ratio = res_x / res_y;

		scalar const tx = 2 * px / res_x - 1;
		scalar const ty = 1 - 2 * py / res_y;

		vector const dir = normalized(z + x * tx * ratio + y * ty);

		return { position, dir };
	}
};

scene test_scene ( )
{
	scene s;

	auto fill_last_triangle_normals = [&s]{
		auto const & t = s.triangle_vertices.back();

		auto const p0 = s.points[t[0]];
		auto const p1 = s.points[t[1]];
		auto const p2 = s.points[t[2]];

		auto const n = normalized((p1 - p0) ^ (p2 - p0));

		s.normals.push_back(n);
		s.normals.push_back(n);
		s.normals.push_back(n);
	};

	// Front face

	s.points.push_back({-10.f, -10.f, 20.f});
	s.points.push_back({ 10.f, -10.f, 20.f});
	s.points.push_back({-10.f,  10.f, 20.f});
	s.points.push_back({ 10.f,  10.f, 20.f});

	s.normals.push_back({0.f, 0.f, -1.f});
	s.normals.push_back({0.f, 0.f, -1.f});
	s.normals.push_back({0.f, 0.f, -1.f});
	s.normals.push_back({0.f, 0.f, -1.f});

	s.triangle_vertices.push_back({0, 1, 2});
	s.triangle_vertices.push_back({1, 2, 3});

	s.materials.push_back({{0.f, 1.f, 0.f}});

	s.triangle_materials.push_back(0);
	s.triangle_materials.push_back(0);

	// Left face

	s.points.push_back({-10.f, -10.f,  0.f});
	s.points.push_back({-10.f, -10.f, 20.f});
	s.points.push_back({-10.f,  10.f,  0.f});
	s.points.push_back({-10.f,  10.f, 20.f});

	s.normals.push_back({1.f, 0.f, 0.f});
	s.normals.push_back({1.f, 0.f, 0.f});
	s.normals.push_back({1.f, 0.f, 0.f});
	s.normals.push_back({1.f, 0.f, 0.f});

	s.triangle_vertices.push_back({4, 5, 6});
	s.triangle_vertices.push_back({5, 6, 7});

	s.materials.push_back({{1.f, 0.f, 0.f}});

	s.triangle_materials.push_back(1);
	s.triangle_materials.push_back(1);

	// Right face

	s.points.push_back({ 10.f, -10.f,  0.f});
	s.points.push_back({ 10.f, -10.f, 20.f});
	s.points.push_back({ 10.f,  10.f,  0.f});
	s.points.push_back({ 10.f,  10.f, 20.f});

	s.normals.push_back({-1.f, 0.f, 0.f});
	s.normals.push_back({-1.f, 0.f, 0.f});
	s.normals.push_back({-1.f, 0.f, 0.f});
	s.normals.push_back({-1.f, 0.f, 0.f});

	s.triangle_vertices.push_back({8, 9, 10});
	s.triangle_vertices.push_back({9, 10, 11});

	s.materials.push_back({{0.f, 0.f, 1.f}});

	s.triangle_materials.push_back(2);
	s.triangle_materials.push_back(2);

	// Bottom face

	s.points.push_back({ 10.f, -10.f,  0.f});
	s.points.push_back({ 10.f, -10.f, 20.f});
	s.points.push_back({-10.f, -10.f,  0.f});
	s.points.push_back({-10.f, -10.f, 20.f});

	s.normals.push_back({0.f, 1.f, 0.f});
	s.normals.push_back({0.f, 1.f, 0.f});
	s.normals.push_back({0.f, 1.f, 0.f});
	s.normals.push_back({0.f, 1.f, 0.f});

	s.triangle_vertices.push_back({12, 13, 14});
	s.triangle_vertices.push_back({13, 14, 15});

	s.materials.push_back({{1.f, 1.f, 1.f}});

	s.triangle_materials.push_back(3);
	s.triangle_materials.push_back(3);

	// Top face

	s.points.push_back({ 10.f,  10.f,  0.f});
	s.points.push_back({ 10.f,  10.f, 20.f});
	s.points.push_back({-10.f,  10.f,  0.f});
	s.points.push_back({-10.f,  10.f, 20.f});

	s.normals.push_back({0.f, -1.f, 0.f});
	s.normals.push_back({0.f, -1.f, 0.f});
	s.normals.push_back({0.f, -1.f, 0.f});
	s.normals.push_back({0.f, -1.f, 0.f});

	s.triangle_vertices.push_back({16, 17, 18});
	s.triangle_vertices.push_back({17, 18, 19});

	s.materials.push_back({{1.f, 1.f, 1.f}});

	s.triangle_materials.push_back(4);
	s.triangle_materials.push_back(4);

	// Light source

	float const size = 0.1f;

	s.points.push_back({10.f - size, 10.f, 20.f});
	s.points.push_back({10.f, 10.f - size, 20.f});
	s.points.push_back({10.f, 10.f, 20.f - size});

	vector const normal = geom::normalized(vector{-1.f, -1.f, -1.f});

	s.normals.push_back(normal);
	s.normals.push_back(normal);
	s.normals.push_back(normal);

	s.triangle_vertices.push_back({20, 21, 22});

	scalar const light = 16.f;
	s.materials.push_back({{1.f, 1.f, 1.f}, {light, light, light}});
	s.triangle_materials.push_back(5);

	// Reflecting triangle

	s.points.push_back({{-8.f, -5.f, 12.f }});
	s.points.push_back({{-8.f, -5.f, 18.f }});
	s.points.push_back({{-2.f, -6.f, 12.f }});

	s.triangle_vertices.push_back({23, 24, 25});

	fill_last_triangle_normals();

	s.materials.push_back({{0.8f, 0.5f, 0.2f}, {0.f, 0.f, 0.f}, 0.3f, 0.0f});
	s.triangle_materials.push_back(6);

	return s;

	// Sphere

	{

		index const base = s.points.size();
		index const mat = s.materials.size();

		s.materials.push_back({{0.8f, 0.5f, 0.2f}, {0.f, 0.f, 0.f}, 0.3f, 0.0f});
//		s.materials.push_back({{0.8f, 0.5f, 0.2f}});

		index const sphere_res = 12;
		sphere const sph { {-5.f, -5.f, 15.f }, 3.f };

		// u = [0 .. 2pi]
		// v = [-pi/2 .. pi/2]
		auto const push = [&](scalar u, scalar v)
		{
			vector const n {std::cos(u) * std::cos(v), std::sin(v), std::sin(u) * std::cos(v)};
			s.points.push_back(sph.origin + n * sph.radius);
			s.normals.push_back(n);
		};

		auto const idx = [=](index u, index v)
		{
			u %= 2 * sphere_res;
			return base + 2 + (v - 1) * 2 * sphere_res + u;
		};

		push(0.f, -pi / 2); // 0
		push(0.f,  pi / 2); // 1

		for (index v = 1; v < sphere_res; ++v)
		{
			scalar const av = (scalar(v) / sphere_res - 0.5f) * pi;

			for (index u = 0; u < 2 * sphere_res; ++u)
			{
				scalar const au = (u * pi) / sphere_res;

				push(au, av);
			}
		}

		// Bottom cap

		for (index u = 0; u < 2 * sphere_res; ++u)
		{
			s.triangle_vertices.push_back({base, idx(u, 1), idx(u + 1, 1)});
			s.triangle_materials.push_back(mat);
		}

		// Middle stripes

		for (index v = 1; v < sphere_res - 1; ++v)
		{
			for (index u = 0; u < 2 * sphere_res; ++u)
			{
				s.triangle_vertices.push_back({idx(u, v), idx(u + 1, v), idx(u, v + 1)});
				s.triangle_vertices.push_back({idx(u + 1, v), idx(u, v + 1), idx(u + 1, v + 1)});
				s.triangle_materials.push_back(mat);
				s.triangle_materials.push_back(mat);
			}
		}

		// Top cap

		for (index u = 0; u < 2 * sphere_res; ++u)
		{
			s.triangle_vertices.push_back({base + 1, idx(u, sphere_res - 1), idx(u + 1, sphere_res - 1)});
			s.triangle_materials.push_back(mat);
		}

	}

	return s;
}

camera default_camera ( )
{
	camera c;
	c.position = {0.f, 0.f, 0.f};
	c.x = {1.f, 0.f, 0.f};
	c.y = {0.f, 1.f, 0.f};
	c.z = {0.f, 0.f, 1.f};
	c.fov_x = 1.f;
	return c;
}

struct accelerator
{
	scene const & s;

	std::vector<geom::matrix<scalar, 3, 3>> triangle_inverse_matrix;

	std::vector<index> emitting_triangles;

	enum class bvh_node_type : index
	{
		split_x = 0,
		split_y = 1,
		split_z = 2,
		leaf = 3
	};

	struct bvh_node
	{
		bvh_node (bvh_node_type type, index value)
		{
			data = static_cast<index>(type) | (value << 2);
		}

		bvh_node_type type ( ) const
		{
			return static_cast<bvh_node_type>(data & 0b11);
		}

		void type (bvh_node_type t)
		{
			data = (data & (~index(0b11))) | static_cast<index>(t);
		}

		index value ( ) const
		{
			return (data >> 2);
		}

		void value (index v)
		{
			data = (data & index(0b11)) | (v << 2);
		}

	private:
		index data;
	};

	std::vector<bvh_node> bvh_nodes;
	std::vector<box> bvh_bboxes;
	std::vector<index> bvh_triangle_index_ranges;
	std::vector<index> bvh_triangle_indices;

	accelerator (scene const & s);

	interval intersect (ray const & r, index i, scalar & u, scalar & v) const;

private:

	void fill_triangle_inverse_matrices ( );

	void fill_emitting_triangles ( );

	void build_bvh (index * begin, index * end);
};

accelerator::accelerator (scene const & s)
	: s(s)
{
	fill_triangle_inverse_matrices();

	fill_emitting_triangles();

	std::vector<index> triangle_indices(s.triangle_vertices.size());
	std::iota(triangle_indices.begin(), triangle_indices.end(), index(0));

	bvh_nodes.reserve(s.triangle_vertices.size());
	bvh_bboxes.reserve(s.triangle_vertices.size());
	bvh_triangle_index_ranges.reserve(s.triangle_vertices.size());
	bvh_triangle_indices.reserve(s.triangle_vertices.size());

	build_bvh(triangle_indices.data(), triangle_indices.data() + triangle_indices.size());
}

void accelerator::fill_triangle_inverse_matrices ( )
{
	triangle_inverse_matrix.resize(s.triangle_vertices.size());

	for (std::size_t i = 0; i < s.triangle_vertices.size(); ++i)
	{
		auto const & tri = s.triangle_vertices[i];

		auto const & p0 = s.points[tri[0]];
		auto const & p1 = s.points[tri[1]];
		auto const & p2 = s.points[tri[2]];

		auto const e1 = p1 - p0;
		auto const e2 = p2 - p0;
		auto const n = e1 ^ e2;

		geom::matrix<scalar, 3, 3> m;

		m[0][0] = e1[0];
		m[1][0] = e1[1];
		m[2][0] = e1[2];

		m[0][1] = e2[0];
		m[1][1] = e2[1];
		m[2][1] = e2[2];

		m[0][2] = n[0];
		m[1][2] = n[1];
		m[2][2] = n[2];

		triangle_inverse_matrix[i] = inverse(m);
	}
}

void accelerator::fill_emitting_triangles ( )
{
	for (std::size_t i = 0; i < s.triangle_vertices.size(); ++i)
	{
		if (s.materials[s.triangle_materials[i]].emission != vector::zero())
			emitting_triangles.push_back(i);
	}
}

void accelerator::build_bvh (index * begin, index * end)
{
	static const float traversal_cost = 1.f;
	static const float intersection_cost = 8.f;
	
	
}

interval accelerator::intersect (ray const & r, index i, scalar & u, scalar & v) const
{
	auto const & m = triangle_inverse_matrix[i];
	auto const & p = s.points[s.triangle_vertices[i][0]];

	vector const o = m * (r.origin - p);
	vector const d = m * r.direction;

	scalar const t = - o[2] / d[2];

	u = o[0] + t * d[0];
	v = o[1] + t * d[1];

	if (u >= 0 && v >= 0 && u + v <= 1)
		return interval{t};

	return {};
}

vector raycast (scene const & s, accelerator const & acc, ray const & r, rng & gen, std::size_t depth)
{
	static constexpr vector ambient { 10.f, 10.f, 10.f };

	if (depth == 0)
		return ambient;

	scalar min_t = inf;
	std::optional<index> min_ti;
	vector n;

	for (std::size_t i = 0; i < s.triangle_vertices.size(); ++i)
	{
		auto tri = s.triangle_vertices[i];

		scalar u, v;

		auto intersection = acc.intersect(r, i, u, v);

		intersection &= interval(0, inf);

		if (!intersection.empty() && intersection.inf < min_t)
		{
			min_t = intersection.inf;
			min_ti = i;
			n = normalized(s.normals[tri[0]] * (1 - u - v) + s.normals[tri[1]] * u + s.normals[tri[2]] * v);
		}
	}

	if (min_ti)
	{
		material const & mat = s.materials[s.triangle_materials[*min_ti]];

		ray new_r;
		new_r.origin = r(min_t);

		vector mask;

		scalar const scatter_type = random(gen);

		if (scatter_type < mat.reflectance)
		{
			// Reflection

			new_r.direction = r.direction - scalar(2) * n * (n * r.direction);
			new_r.direction += random_unit_ball(gen) * mat.reflection_fuzziness;
			new_r.direction = normalized(new_r.direction);

			mask = mat.color;
		}
		else
		{
			// Diffusion

			constexpr scalar const importance_sampling_threshold = 1.0f;

			scalar const do_importance_sampling = random(gen);

			if (do_importance_sampling < importance_sampling_threshold)
			{
				// Usual diffusion

				new_r.direction = random_unit_halfsphere(gen, n);
				mask = mat.color * (new_r.direction * n) / pi / importance_sampling_threshold;
			}
			else
			{
				// Importance sampling

				std::size_t const i = random_index(gen, acc.emitting_triangles.size());

				auto const & t = s.triangle_vertices[acc.emitting_triangles[i]];

				auto [u, v] = random_triangle(gen);

				auto const p0 = s.points[t[0]];
				auto const p1 = s.points[t[1]];
				auto const p2 = s.points[t[2]];

				scalar const area = 1;//std::abs(geom::det(p1 - p0, p2 - p0)) * scalar(0.5);

				auto const p = p0 + u * (p1 - p0) + v * (p2 - p0);

				new_r.direction = geom::normalized(p - new_r.origin);

				if (new_r.direction * n <= scalar(0))
					return mat.emission;

				scalar const distance_sqr = geom::distance_sqr(p, new_r.origin);

				if (distance_sqr < scalar(0.0001))
					return mat.emission;

				auto const n0 = s.normals[t[0]];
				auto const n1 = s.normals[t[1]];
				auto const n2 = s.normals[t[2]];

				auto const light_n = geom::normalized((1 - u - v) * n0 + u * n1 + v * n2);

				scalar const cos_angle = std::abs(std::cos(light_n * new_r.direction));

				mask = mat.color * (new_r.direction * n) * cos_angle * area / distance_sqr / (scalar(1) - importance_sampling_threshold) * scalar(acc.emitting_triangles.size());
			}
		}

		new_r.origin += scalar(0.0001) * new_r.direction;

		return mat.emission + mult(mask, raycast(s, acc, new_r, gen, depth - 1));
	}

	return ambient;
}

void render (scene const & s, accelerator const & acc, camera const & c, rng gen, std::size_t samples, std::size_t depth, texture & output)
{
	std::atomic<std::size_t> progress { 0 };

	std::size_t const work_thread_count = std::thread::hardware_concurrency();

	std::vector<std::thread> work_threads;

	for (std::size_t t = 0; t < work_thread_count; ++t)
	{
		work_threads.emplace_back([&, t, gen]() mutable {

			std::size_t const y_begin = (t * output.dimensions[1]) / work_thread_count;
			std::size_t const y_end = ((t + 1) * output.dimensions[1]) / work_thread_count;

			for (std::size_t y = y_begin; y < y_end; ++y)
			{
				for (std::size_t x = 0; x < output.dimensions[0]; ++x)
				{
					vector color = vector::zero();

					for (std::size_t sample = 0; sample < samples; ++sample)
					{
						auto r = c.ray(x + random(gen), y + random(gen), output.dimensions[0], output.dimensions[1]);
						color += raycast(s, acc, r, gen, depth);

						progress.fetch_add(1);
					}

					output.at(x, y) = geom::cast<byte>(geom::clamp(color / scalar(samples), color_box) * scalar(255));
				}
			}
		});
	}

	std::size_t const total_progress = output.dimensions[0] * output.dimensions[1] * samples;

	using clock = std::chrono::high_resolution_clock;

	auto last_time = clock::now();
	std::size_t last_progress = 0;
	std::size_t current_progress;

	util::moving_average<std::chrono::nanoseconds> avg(4);

	std::this_thread::sleep_for(std::chrono::milliseconds{100});

	while ((current_progress = progress.load()) < total_progress)
	{
		auto current_time = clock::now();

		auto const inv_speed = (current_time - last_time) / (current_progress - last_progress);

		std::size_t const remaining_progress = total_progress - current_progress;

		avg.push(remaining_progress * inv_speed);

		std::cerr << util::pretty(avg.average()) << " left\n";

		last_time = current_time;
		last_progress = current_progress;

		std::this_thread::sleep_for(std::chrono::seconds{1});
	}

	for (std::thread & th : work_threads)
		th.join();
}

void write_ppm (texture const & t, std::ostream & o)
{
	o << "P6 " << t.dimensions[0] << ' ' << t.dimensions[1] << " 255\n";
	o.write(reinterpret_cast<const char *>(t.data()), t.dimensions[0] * t.dimensions[1] * sizeof(pixel));
}

int main (int argc, char ** argv)
{
	if (argc != 4)
	{
		std::cerr << "Usage: " << argv[0] << " <width> <height> <samples>\n";
		return 0;
	}

	std::size_t const width  = std::atoi(argv[1]);
	std::size_t const height = std::atoi(argv[2]);
	std::size_t const samples = std::atoi(argv[3]);

	texture output {{width, height}};

	using clock = std::chrono::high_resolution_clock;

	auto checkpoint0 = clock::now();

	auto scene = test_scene();

	auto checkpoint1 = clock::now();

	std::cerr << "Scene building took " << util::pretty(checkpoint1 - checkpoint0, std::chrono::seconds{1}) << '\n';

	accelerator acc(scene);

	auto checkpoint2 = clock::now();

	std::cerr << "Acceleration structures building took " << util::pretty(checkpoint2 - checkpoint1, std::chrono::seconds{1}) << '\n';

	render(scene, acc, default_camera(), rng{}, samples, 3, output);

	auto checkpoint3 = clock::now();

	std::cerr << "Rendering took " << util::pretty(checkpoint3 - checkpoint2, std::chrono::seconds{1}) << '\n';

	write_ppm(output, std::cout);
}
