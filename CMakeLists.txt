cmake_minimum_required(VERSION 3.8)
project(raytracer)

set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -march=native -ffast-math -fno-stack-protector ")
set(CMAKE_CXX_STANDARD 17)

find_package(OpenMP REQUIRED)

add_subdirectory(geom)
add_subdirectory(util)

add_executable(render main.cpp)
target_link_libraries(render geom OpenMP::OpenMP_CXX)

add_executable(render2 main2.cpp)
target_link_libraries(render2 geom util)
