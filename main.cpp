#include <geom/alias/vector.hpp>
#include <geom/alias/point.hpp>
#include <geom/alias/matrix.hpp>
#include <geom/alias/interval.hpp>
#include <geom/alias/rectangle.hpp>
#include <geom/alias/ray.hpp>
#include <geom/transform/homogeneous.hpp>
#include <geom/operations/interval.hpp>
#include <geom/operations/rectangle.hpp>
#include <geom/operations/intersect/ray_rectangle.hpp>
#include <geom/primitives/ndarray.hpp>

#include <memory>
#include <random>
#include <chrono>
#include <iostream>
#include <mutex>
#include <chrono>

geom::vector_3d mult (geom::vector_3d const & a, geom::vector_3d const & b)
{
	return { a[0] * b[0], a[1] * b[1], a[2] * b[2] };
}

geom::vector_3d clamp (geom::vector_3d const & c)
{
	static constexpr geom::interval_d i { 0.0, 1.0 };
	return { clamp(c[0], i), clamp(c[1], i), clamp(c[2], i) };
}

struct rng
{
	virtual std::shared_ptr<rng> clone ( ) const = 0;

	virtual double uniform ( ) = 0;

	virtual double uniform (geom::interval_d const & range)
	{
		return range(uniform());
	}

	virtual geom::vector_3d uniform_ball (double radius = 1.0)
	{
		geom::vector_3d v;

		while (true)
		{
			v[0] = uniform({-1.0, 1.0});
			v[1] = uniform({-1.0, 1.0});
			v[2] = uniform({-1.0, 1.0});

			if (geom::norm(v) <= 1.0)
			{
				return v * radius;
			}
		}
	}

	virtual ~ rng ( ) { }
};

using rng_ptr = std::shared_ptr<rng>;

struct default_rng
	: rng
{
	default_rng (std::size_t seed)
		: r(seed)
	{ }

	default_rng ( )
		: default_rng(std::chrono::system_clock::now().time_since_epoch().count())
	{ }

	default_rng (std::default_random_engine const & r)
		: r(r)
	{ }

	rng_ptr clone ( ) const override
	{
		return std::make_shared<default_rng>(r);
	}

	double uniform ( ) override
	{
		return std::uniform_real_distribution<double>()(r);
	}

private:
	std::default_random_engine r;
};

struct intersection
{
	double t;
	geom::vector_3d normal;
};

struct shape
{
	virtual std::optional<intersection> intersect (geom::ray_3d const & ray, geom::interval_d const & range) const = 0;

	virtual geom::rectangle_3d bbox ( ) const = 0;

	virtual ~ shape ( ) { }
};

using shape_ptr = std::shared_ptr<shape>;

struct material
{
	virtual geom::vector_3d color ( ) const
	{
		return { 0.0, 0.0, 0.0 };
	}

	virtual geom::vector_3d emission ( ) const
	{
		return { 0.0, 0.0, 0.0 };
	}

	virtual double reflectance ( ) const
	{
		return 0.0;
	}

	virtual double reflectance_fuzziness ( ) const
	{
		return 0.0;
	}

	virtual double transmittance ( ) const
	{
		return 0.0;
	}

	virtual double refractive_index ( ) const
	{
		return 1.0;
	}

	virtual ~ material ( ) { }
};

using material_ptr = std::shared_ptr<material>;

struct object
{
	shape_ptr shape;
	material_ptr material;
};

struct plane
	: shape
{
	geom::vector_4d func;

	plane (geom::vector_4d func)
		: func(func)
	{ }

	std::optional<intersection> intersect (geom::ray_3d const & ray, geom::interval_d const & range) const override
	{
		double t = - (func * homogeneous(ray.origin)) / (func * homogeneous(ray.direction));

		if (geom::contains(range, t))
			return intersection{t, normal()};

		return std::nullopt;
	}

	geom::rectangle_3d bbox ( ) const override
	{
		return geom::rectangle_3d::full();
	}

private:

	geom::vector_3d normal ( ) const
	{
		geom::vector_3d n;
		n[0] = func[0];
		n[1] = func[1];
		n[2] = func[2];
		return normalized(n);
	}
};

std::shared_ptr<plane> make_plane (geom::vector_4d func)
{
	return std::make_shared<plane>(func);
}

struct sphere
	: shape
{
	geom::point_3d center;
	double radius;

	sphere (geom::point_3d center, double radius)
		: center(center)
		, radius(radius)
	{ }

	std::optional<intersection> intersect (geom::ray_3d const & ray, geom::interval_d const & range) const override
	{
		double const a = geom::norm_sqr(ray.direction);
		double const b_2 = ray.direction * (ray.origin - center);
		double const c = geom::norm_sqr(ray.origin - center) - radius * radius;

		double const D = b_2 * b_2 - a * c;

		if (D < 0.0)
			return std::nullopt;

		double const sqrtD = std::sqrt(D);

		double const t1 = (- b_2 - sqrtD) / a;
		double const t2 = (- b_2 + sqrtD) / a;

		if (contains(range, t1))
		{
			return intersection{t1, normal_at(ray(t1))};
		}

		if (contains(range, t2))
		{
			return intersection{t2, normal_at(ray(t2))};
		}

		return std::nullopt;
	}

	geom::rectangle_3d bbox ( ) const override
	{
		return geom::rectangle_3d {{
			{center[0] - radius, center[0] + radius},
			{center[1] - radius, center[1] + radius},
			{center[2] - radius, center[2] + radius},
		}};
	}

private:

	geom::vector_3d normal_at (geom::point_3d const & p) const
	{
		return normalized(p - center);
	}
};

std::shared_ptr<sphere> make_sphere (geom::point_3d const & center, double radius)
{
	return std::make_shared<sphere>(center, radius);
}

struct diffuse_material
	: material
{
	geom::vector_3d c;

	diffuse_material (geom::vector_3d const & c)
		: c(c)
	{ }

	geom::vector_3d color ( ) const override
	{
		return c;
	}
};

std::shared_ptr<diffuse_material> make_diffuse_material (geom::vector_3d const & c)
{
	return std::make_shared<diffuse_material>(c);
}

struct emitting_material
	: material
{
	geom::vector_3d e;

	emitting_material (geom::vector_3d const & e)
		: e(e)
	{ }

	geom::vector_3d emission ( ) const override
	{
		return e;
	}
};

std::shared_ptr<emitting_material> make_emitting_material (geom::vector_3d const & e)
{
	return std::make_shared<emitting_material>(e);
}

struct mirror_material
	: material
{
	double f;
	geom::vector_3d c;
	double r;

	mirror_material (double f, geom::vector_3d const & c, double r)
		: f(f)
		, c(c)
		, r(r)
	{ }

	geom::vector_3d color ( ) const override
	{
		return c;
	}

	double reflectance ( ) const override
	{
		return r;
	}

	double reflectance_fuzziness ( ) const override
	{
		return f;
	}
};

std::shared_ptr<mirror_material> make_mirror_material (double f = 0.0, geom::vector_3d const & c = { 1.0, 1.0, 1.0 }, double r = 1.0)
{
	return std::make_shared<mirror_material>(f, c, r);
}

struct glass_material
	: material
{
	double r;
	double t;
	geom::vector_3d c;

	glass_material (double r, double t, geom::vector_3d const & c = { 1.0, 1.0, 1.0 })
		: r(r)
		, t(t)
		, c(c)
	{ }

	geom::vector_3d color ( ) const override
	{
		return c;
	}

	double transmittance ( ) const override
	{
		return t;
	}

	double refractive_index ( ) const override
	{
		return r;
	}
};

std::shared_ptr<glass_material> make_glass_material (double r, double t, geom::vector_3d const & c = { 1.0, 1.0, 1.0 })
{
	return std::make_shared<glass_material>(r, t, c);
}

struct aggregate
{
	virtual std::optional<std::pair<intersection, material_ptr>> intersect (geom::ray_3d const & ray, geom::interval_d const & range, bool only_emissive = false) const = 0;

	virtual ~ aggregate ( ) { }
};

std::optional<std::pair<intersection, material_ptr>> best (std::optional<std::pair<intersection, material_ptr>> i1, std::optional<std::pair<intersection, material_ptr>> i2)
{
	if (i1 && i2)
	{
		if (i1->first.t < i2->first.t)
			return std::move(i1);
		else
			return std::move(i2);
	}

	if (i1)
	{
		return std::move(i1);
	}

	return std::move(i2);
}

void update (std::optional<std::pair<intersection, material_ptr>> & res, std::optional<intersection> i, material_ptr mat)
{
	res = best(res, i ? std::optional(std::make_pair(*i, mat)) : std::nullopt);
}

using aggregate_ptr = std::shared_ptr<aggregate>;

struct linear_aggregate
	: aggregate
{
	std::vector<object> objects;

	linear_aggregate (std::vector<object> objects)
		: objects(std::move(objects))
	{ }

	std::optional<std::pair<intersection, material_ptr>> intersect (geom::ray_3d const & ray, geom::interval_d const & range, bool only_emissive) const override
	{
		std::optional<std::pair<intersection, material_ptr>> result;

		for (object const & o : objects)
		{
			if (!only_emissive || o.material->emission() != geom::vector_3d::zero())
				update(result, o.shape->intersect(ray, range), o.material);
		}

		return result;
	}
};

struct rtree_aggregate
	: aggregate
{
	struct node
	{
		std::optional<object> obj;
		geom::rectangle_3d bbox;

		std::unique_ptr<node> left, right;
	};

	struct object_and_bbox
	{
		object o;
		geom::rectangle_3d b;
	};

	using node_ptr = std::unique_ptr<node>;

	rtree_aggregate (std::vector<object> input)
	{
		auto const is_bounded = [](object const & o){ return !o.shape->bbox().infinite(); };

		auto it = std::partition(input.begin(), input.end(), is_bounded);

		std::vector<object> unbounded_objects(it, input.end());

		input.erase(it, input.end());

		unbounded = std::make_shared<linear_aggregate>(std::move(unbounded_objects));

		std::vector<object_and_bbox> objects;
		objects.reserve(input.size());
		for (auto & o : input)
		{
			auto bbox = o.shape->bbox();
			objects.push_back({std::move(o), bbox});
		}

		root = build(objects.data(), objects.data() + objects.size());
	}

	std::optional<std::pair<intersection, material_ptr>> intersect (geom::ray_3d const & ray, geom::interval_d const & range, bool) const override
	{
		return best(unbounded->intersect(ray, range), intersect(ray, range, *root));
	}

private:

	aggregate_ptr unbounded;
	node_ptr root;

	node_ptr merge (node_ptr left, node_ptr right)
	{
		node_ptr result = std::make_unique<node>();
		result->left = std::move(left);
		result->right = std::move(right);
		result->bbox = result->left->bbox | result->right->bbox;
		return result;
	}

	node_ptr leaf (object_and_bbox & ob)
	{
		node_ptr result = std::make_unique<node>();
		result->obj = ob.o;
		result->bbox = ob.b;
		return result;
	}

	node_ptr build (object_and_bbox * begin, object_and_bbox * end)
	{
		auto const count = end - begin;

		if (count == 1)
		{
			return leaf(*begin);
		}
		else if (count == 2)
		{
			return merge(leaf(*begin), leaf(*std::next(begin)));
		}

		geom::point_3d center = geom::point_3d::zero();

		for (auto * p = begin; p != end; ++p)
		{
			auto const c = p->b.center();
			center[0] += c[0];
			center[1] += c[1];
			center[2] += c[2];
		}

		center[0] /= count;
		center[1] /= count;
		center[2] /= count;

		int metric[3];

		for (std::size_t d = 0; d < 3; ++d)
		{
			int left = 0;
			int middle = 0;
			int right = 0;

			for (auto * p = begin; p != end; ++p)
			{
				if (p->b[d].sup < center[d])
					++left;
				else if (p->b[d].inf < center[d])
					++middle;
				else
					++right;
			}

			metric[d] = middle + std::abs(left - right);
		}

		std::size_t split_d = 0;
		for (std::size_t d = 1; d < 3; ++d)
		{
			if (metric[d] < metric[split_d])
				split_d = d;
		}

		auto split_value = center[split_d];

		auto const is_left = [=](object_and_bbox const & ob)
		{
			return ob.b[split_d].sup < split_value;
		};

		auto const is_middle = [=](object_and_bbox const & ob)
		{
			return ob.b[split_d].inf < split_value;
		};

		auto left_end = std::partition(begin, end, is_left);

		auto middle_end = std::partition(left_end, end, is_middle);

		if (left_end == begin || middle_end == end)
		{
			// Axis-splitting didn't work
			// Just split in half
			auto middle = begin + (count / 2);
			return merge(build(begin, middle), build(middle, end));
		}
		else
		{
			return merge(build(begin, middle_end), build(left_end, end));
		}
	}

	std::optional<std::pair<intersection, material_ptr>> intersect (geom::ray_3d const & ray, geom::interval_d const & range, node const & n) const
	{
		auto irange = geom::intersect(ray, n.bbox);

		if (!((irange & range).empty()))
		{
			std::optional<std::pair<intersection, material_ptr>> result;

			if (n.obj)
			{
				update(result, n.obj->shape->intersect(ray, range), n.obj->material);
			}

			if (n.left)
			{
				result = best(result, intersect(ray, range, *n.left));
			}

			if (n.right)
			{
				result = best(result, intersect(ray, range, *n.right));
			}

			return result;
		}

		return std::nullopt;
	}
};

struct sampler
{
	rng_ptr rand;
	aggregate_ptr scene;

	template <typename Callback>
	void render (std::size_t width, std::size_t height, Callback && cb, std::size_t samples, std::size_t max_depth) const
	{
		using clock = std::chrono::high_resolution_clock;

		std::size_t progress = 0;

		auto start_time = clock::now();

		#pragma omp parallel
		{
			auto rand = this->rand->clone();

			#pragma omp for
			for (std::size_t x = 0; x < width; ++x)
			{
				for (std::size_t y = 0; y < height; ++y)
				{
					geom::vector_3d c { 0.0, 0.0, 0.0 };

					std::vector<double> refractive_index_stack;

					for (std::size_t s = 0; s < samples; ++s)
					{
						geom::ray_3d ray;
						ray.origin = { 0.0, 0.0, 9.9 };
						ray.direction[0] = ((2.0 * (x + rand->uniform())) - (1.0 * width)) / height;
						ray.direction[1] = 1.0 - (2.0 * (y + rand->uniform())) / height;
						ray.direction[2] = -1.0;

						ray.direction = geom::normalized(ray.direction);

						refractive_index_stack.assign(1, 1.0);

						c += raycast(ray, max_depth, refractive_index_stack);
					}

					cb(x, y, clamp(c / static_cast<double>(samples)));
				}

				#pragma omp critical
				{
					progress += height;
					double p = (progress * 1.0 / width) / height;

					double time_estimate = (std::chrono::duration_cast<std::chrono::duration<double>>(clock::now() - start_time).count() * (1 - p) / p);

					std::cerr << (p * 100.0) << "%, " << time_estimate << "s left\n";
				}
			}
		}
	}

	geom::vector_3d raycast (geom::ray_3d const & ray, std::size_t max_depth, std::vector<double> & refractive_index_stack) const
	{
		std::optional<std::pair<intersection, material_ptr>> tr = scene->intersect(ray, {0.0, std::numeric_limits<double>::infinity()});

		if (!tr)
			return geom::vector_3d::zero();

		intersection i = tr->first;
		material_ptr mat = tr->second;

		if (max_depth == 0)
			return mat->emission();

		double scatter_type_sample = rand->uniform();

		geom::ray_3d next_ray;
		geom::vector_3d next_ray_mask;

		next_ray.origin = ray(i.t);

		if (scatter_type_sample < mat->reflectance())
		{
			next_ray.direction = ray.direction - 2.0 * i.normal * (i.normal * ray.direction);
			next_ray.direction += rand->uniform_ball() * mat->reflectance_fuzziness();

			next_ray_mask = mat->color();
		}
		else if (scatter_type_sample < mat->reflectance() + mat->transmittance())
		{
			double refractive_ratio;
			double theta1;

			if (ray.direction * i.normal > 0.0)
			{
				refractive_ratio = mat->refractive_index() / refractive_index_stack.back();
				theta1 = std::acos(ray.direction * i.normal);
			}
			else
			{
				refractive_ratio = refractive_index_stack.back() / mat->refractive_index();
				theta1 = std::acos(-ray.direction * i.normal);
			}

			double theta2 = std::asin(refractive_ratio * std::sin(theta1));

			geom::vector_3d r_normal = geom::normalized(i.normal * (ray.direction * i.normal));
			geom::vector_3d r_tangent = geom::normalized(ray.direction - r_normal * (ray.direction * r_normal));

			next_ray.direction = std::cos(theta2) * r_normal + std::sin(theta2) * r_tangent;

			next_ray_mask = mat->color();

//			return geom::vector_3d{1.0, 1.0, 0.0} * (theta1);
		}
		else
		{
			next_ray.direction = normalized(i.normal + rand->uniform_ball());

			next_ray_mask = mat->color() * (next_ray.direction * i.normal);
		}

		next_ray.direction = geom::normalized(next_ray.direction);
		next_ray.origin += next_ray.direction * 0.001;

		if (ray.direction * i.normal > 0.0)
		{
			if (refractive_index_stack.size() > 1)
				refractive_index_stack.pop_back();
		}
		else
		{
			refractive_index_stack.push_back(mat->refractive_index());
		}

		return mat->emission() + mult(next_ray_mask, raycast(next_ray, max_depth - 1, refractive_index_stack));
	}
};

int main (int argc, char ** argv)
{
	if (argc != 4)
	{
		std::cout << "Usage: " << argv[0] << " <width> <height> <samples>\n";
		return 0;
	}

	std::size_t const width  = std::atoi(argv[1]);
	std::size_t const height = std::atoi(argv[2]);
	std::size_t const samples = std::atoi(argv[3]);

	sampler s;

	s.rand = std::make_shared<default_rng>();

	std::vector<object> objects;

	objects.push_back({
		make_plane({1.0, 0.0, 0.0, 10.0}),
		make_diffuse_material({1.0, 0.0, 0.0})
	});

	objects.push_back({
		make_plane({-1.0, 0.0, 0.0, 10.0}),
		make_diffuse_material({0.0, 0.0, 1.0})
	});

	objects.push_back({
		make_plane({0.0, 0.0, 1.0, 10.0}),
		make_diffuse_material({0.0, 1.0, 0.0})
	});

	objects.push_back({
		make_plane({0.0, 0.0, -1.0, 10.0}),
		make_diffuse_material({1.0, 1.0, 1.0})
	});

	objects.push_back({
		make_plane({0.0, -1.0, 0.0, 10.0}),
		make_emitting_material({1.0, 1.0, 1.0}),
	});

	objects.push_back({
		make_plane({0.0, 1.0, 0.0, 10.0}),
//		make_mirror_material(0.0, { 1.0, 1.0, 1.0 }, 0.3)
		make_diffuse_material({1.0, 1.0, 1.0})
	});

	objects.push_back({
		make_sphere({-4.0, -4.0, -2.0}, 3.0),
		//make_diffuse_material({1.0, 1.0, 1.0})
		//make_glass_material(1.0, 0.75, {1.0, 1.0, 1.0})
		make_mirror_material(0.0, {0.75, 0.75, 1.0}, 0.5)
	});

	objects.push_back({
		make_sphere({4.0, -4.0, -2.0}, 3.0),
		//make_diffuse_material({1.0, 1.0, 1.0})
		make_mirror_material(0.3, {0.8, 0.5, 0.2}, 0.25)
	});

/*
	for (int i = 0; i < 10; ++i)
	{
		geom::interval_d bb { -10.0, 10.0 };

		objects.push_back({
			make_sphere({s.rand->uniform(bb), s.rand->uniform(bb), s.rand->uniform({ -10.0, 0.0 })}, 1.0),
			make_diffuse_material({s.rand->uniform(), s.rand->uniform(), s.rand->uniform()})
		});
	}
*/

	s.scene = std::make_shared<linear_aggregate>(std::move(objects));
//	s.scene = std::make_shared<rtree_aggregate>(std::move(objects));

	geom::ndarray<geom::vector_3ub, 2> pixels( { width, height } );

	s.render(width, height, [&](std::size_t x, std::size_t y, geom::vector_3d const & c){
		pixels.at(x, y)[0] = static_cast<unsigned char>(c[0] * 255);
		pixels.at(x, y)[1] = static_cast<unsigned char>(c[1] * 255);
		pixels.at(x, y)[2] = static_cast<unsigned char>(c[2] * 255);
	}, samples, 4);

	std::cout << "P6 " << width << ' ' << height << " 255\n";
	std::cout.write(reinterpret_cast<const char *>(pixels.data()), width * height * sizeof(geom::vector_3ub));
}
